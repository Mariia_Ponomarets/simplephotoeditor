﻿$(document).ready(function () { 

  var content = [
    {
      uid: 10012312344,
      text: 'ВЬЕТНАМКИ МОДЕЛЬ 8A9',
      price: 399,
      img:'https://intertop.ua/load/8A9/IMG_6809.jpg'
    },
    {
      uid: 10167846578,
      text: 'TOMMY HILFIGER ВЬЕТНАМКИ МОДЕЛЬ TE529',
      price: 999,
      img: 'https://intertop.ua/load/TE529/IMG_9130.jpg'
    },
    {
      uid: 10167846599,
      text: 'TOMMY HILFIGER ПОЛУБОТИНКИ МОДЕЛЬ TD842',
      price: 1799,
      img: 'https://intertop.ua/load/TD842/IMG_7815.jpg'
    }
  ];

  //заповнюєм список
  for (var i = 0; i < content.length; i++) {
    var tmp = $('<option>').append(content[i].text);
    tmp.attr('value',content[i].uid);
    $('#product-input').append(tmp);
  };

  //обираєм елемент
  $('#product-button').click(function () {
    var chooseElem = $('#product-input option:selected').val();
    var el = findValueFromList(chooseElem);
    setConteinerSize(el.img)
    addInfo(el);
  });

  //знаходимо обраний елемент
  function findValueFromList(id) {
    for (var i = 0; i < content.length; i++) {
      if (id == content[i].uid) {
        return content[i];
      }
    }
  };

  //додаєму інфу
  function addInfo(el) {
    var titles = $('.title-product');
    var barcodes = $('.barcode');
    var prices = $('.price');
    var priceText = el.price+' грн.'
    addText(titles, el.text);
    addText(barcodes, el.uid);
    addText(prices, priceText);
  }
  function addText(arr,text) {
    for (var i = 0; i < arr.length; i++) {
        arr[i].innerHTML = text;
    }
  }

  //визначаємо розмір контейнера
  function setConteinerSize(url) {
    var img = new Image();
    img.src = url;
    var h, w;
    $(img).load(function () { 
  
      if ($('.img-editor').height() < img.height && $('.img-editor').width() < img.width) {
        if (img.height < img.width) {
          w = $('.img-editor').width();
          h = 'auto';
        }
        else {
          h = $('.img-editor').height();
          w = 'auto';
        }
      }
      else {
        if ($('.img-editor').height() < img.height) {
          w = $('.img-editor').width();
          h = 'auto';
        }
        else {
          if ($('.img-editor').width() < img.width) {
            h = $('.img-editor').height();
            w = 'auto';
          }
          else {
            h = img.height;
            w = img.width;
          }
        }
        setCss(h, w, url);
      }
    })
  };

  //встановлюємо параметри для контейнера
  function setCss(h, w, urlImg) {
    $('#img-box').css({ 'height': h, 'width': w, 'background': 'url('+urlImg+')', 'background-size': 'cover' })
  };


  //додаэмо елементи
  $('.rectangle').click(function () {
    var rec = $(this).clone().appendTo('#img-box');
    rec.removeClass('prev-img');
    addTransform(rec);
  });
  $('.tsilіndr').click(function () {
    var rec = $(this).clone().appendTo('#img-box');
    rec.removeClass('prev-img');
    addTransform(rec);
  });
  $('.drop').click(function () {
    var rec = $(this).clone().appendTo('#img-box');
    rec.removeClass('prev-img');
    addTransform(rec);
  });

  $('.rock').click(function () {
    var rec = $(this).clone().appendTo('#img-box');
    rec.removeClass('prev-img');
    addTransform(rec);
  });
 

  function addTransform(element) {
    element.freetrans({ x: 0, y: 0 });
    element.freetrans('controls', false);
    element.mouseover(function () {
      if (element.children('.close-freetrans').hasClass('hide-elem')) {
        if (element.children('.show-freetrans').hasClass('hide-elem') === true) {
          element.children('.show-freetrans').removeClass('hide-elem');
          element.children('.delete-elem').removeClass('hide-elem');
        }
      }
      else {
        element.children('.show-freetrans').addClass('hide-elem');
      }

    });
    element.mouseleave(function () {
      if (element.children('.show-freetrans').hasClass('hide-elem') === false) {
        element.children('.show-freetrans').addClass('hide-elem');
        element.children('.delete-elem').addClass('hide-elem');
      }
    });

    $('.show-freetrans').click(function () {
      $(this).parent().freetrans('controls', true);
      $(this).addClass('hide-elem');
      $(this).siblings('.close-freetrans').removeClass('hide-elem');
    })

    $('.close-freetrans').click(function () {
      $(this).parent().freetrans('controls', false);
      $(this).addClass('hide-elem');
      $(this).siblings('.delete-elem').addClass('hide-elem');

    });
    $('.delete-elem').click(function () {
      $(this).parent().freetrans('controls', false);
      $(this).parent().remove();
    });
  };


});